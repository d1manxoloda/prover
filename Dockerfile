FROM python:3.6
ARG ENV MTS
ENV MTS=${MTS}
WORKDIR  /code
COPY . /code

RUN pip install --verbose -r requirements.txt

ENTRYPOINT [ "python3" ]
CMD ["app.py"]

EXPOSE 5000